﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class EmptyImageEffect : MonoBehaviour {

    private Material material;

    void Awake()
    {
        material = new Material( Shader.Find("Hidden/EmptyImageEffect"));
    }

    void OnRenderImage(RenderTexture source, RenderTexture dest)
    {
        Graphics.Blit(source, dest, material);
    }
}
