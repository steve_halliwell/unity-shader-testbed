﻿Shader "Custom/EmptyForwardShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf SimpleSpecular fullforwardshadows exclude_path:deferred exclude_path:prepass

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		float4 _Color;
		float _Glossiness;

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			float4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Gloss = _Glossiness;
		}

		//from unity mobile bumpspec MobileBlinnPhong
		inline float4 LightingSimpleSpecular (SurfaceOutput s, float3 lightDir, float3 floatDir, float atten)
		{
			float diff = max (0, dot (s.Normal, lightDir));
			float nh = max (0, dot (s.Normal, floatDir));
			float spec = pow (nh, s.Specular*128) * s.Gloss;
	
			float4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
			UNITY_OPAQUE_ALPHA(c.a);
			return c;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
