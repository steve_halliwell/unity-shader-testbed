﻿Shader "Custom/EmptyForwardUnlitShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		

		//NOTE this is a stupid way of doing unlit but it means we could still get full shadows from an entirely self illum object
		CGPROGRAM
		#pragma surface surf Unlit fullforwardshadows

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		float4 _Color;
		float _Glossiness;

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			float4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}

		inline float4 LightingUnlit (SurfaceOutput s, float3 lightDir, float3 floatDir, float atten)
		{	
			float4 c = 0;
			c.rgb = s.Albedo;
			UNITY_OPAQUE_ALPHA(c.a);
			return c;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
